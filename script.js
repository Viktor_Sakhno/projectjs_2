let array = [];
const sexMask = 0b1; /* 1 bit */
const maritalStatusMask = 0b10; /* 1 bit */
const ageMask = 0b111111100; /* 7 bits, max 127 years */
const numberChildrenMask = 0b111000000000; /* 3 bits, max 7 children */
const higherEducationMask = 0b1000000000000; /* 1 bit */
let n = 2000; /* the number of elements in the array */


fillArr(n);
let newArray = array.filter(firstFilter);
console.log(newArray);
// функція фільтр 1: firstFilter
// функція фільтр 2: secondFilter
// функція фільтр 3: thirdFilter

function fillArr(n) {
    for(let i = 0; i < n; i++) {
        array.push(generateNumber());
    }
}

function generateNumber() {
    return (getRandomInRange(0, 1) << 12) | (getRandomInRange(0, 7) << 9) | (getRandomInRange(0, 127) << 2) | (getRandomInRange(0, 1) << 1) | getRandomInRange(0, 1);
}

function getRandomInRange(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
// функція фільтр 1: неодружені чоловіки з вищою освітою молодше 30 років
function firstFilter (num) {
    return checkSex(num, sexMask)
        & !(checkMaritalStatus(num, maritalStatusMask))
        & checkHigherEducation(num, higherEducationMask)
        & checkAge(num, ageMask) < 30;
}

// функція фільтр 2: одружені жінки в яких 3 і більше дитини
function secondFilter (num) {
    return !(checkSex(num, sexMask))
        & checkMaritalStatus(num, maritalStatusMask)
        & checkNumberChildren(num, numberChildrenMask) >= 3;
}

// функція фільтр 3: неодружені жінки у віці від 25 (включно) до 30 (включно) з вищою освітою і без дітей
function thirdFilter (num) {
    return !(checkSex(num, sexMask))
        & !(checkMaritalStatus(num, maritalStatusMask))
        & checkHigherEducation(num, higherEducationMask)
        & (checkAge(num, ageMask) >= 25 && checkAge(num, ageMask) <= 30)
        & checkNumberChildren(num, numberChildrenMask) < 1;
}

function checkSex(num, mask) {
    return !!(num & mask);
}

function checkMaritalStatus(num, mask) {
    return !!(( num & mask ) >> 1);
}

function checkAge(num, mask) {
    return ( num & mask ) >> 2;
}

function checkNumberChildren(num, mask) {
    return ( num & mask ) >> 9;
}

function checkHigherEducation(num, mask) {
    return !!((num & mask) >> 12);
}
